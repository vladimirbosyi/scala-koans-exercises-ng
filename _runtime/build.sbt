organization := "org.scalakoans"

name := "scala-koans-runtime" 

version := "1.0.1"

scalaVersion := "2.9.1"

libraryDependencies ++= Seq("org.scalatest" %% "scalatest" % "1.8" )
                            
