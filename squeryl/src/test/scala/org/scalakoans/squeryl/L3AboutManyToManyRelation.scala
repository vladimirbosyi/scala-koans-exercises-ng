package org.scalakoans.squeryl

import org.functionalkoans.forscala.support._


import org.squeryl._
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;

object L3ManyToManyScope {

case class Person(val id: Long, val firstName:String, val secondName: String) 
                                                 extends KeyedEntity[Long]
{

}

case class Book(val id: Long, val title: String) 
                                                 extends KeyedEntity[Long]
{

  lazy val authors = DBSchema.authorship.right(this);

}

case class Authorship(val authorId: Long, val bookId: Long)
                                       extends KeyedEntity[CompositeKey2[Long,Long]]
{

  def id = CompositeKey2(authorId, bookId);

}


object DBSchema extends Schema
{
  val persons = table[Person]
  val books = table[Book]
  val authorship = manyToManyRelation(persons,books).via[Authorship](
                     (p,b,a) => (a.authorId === p.id, a.bookId === b.id)
                   )
}

};

class L3AboutManyToManyRelation extends KoanSuite
{

  import L3ManyToManyScope._

  koan("Many to many relations.") {
     SquerylSupport.withTransaction("l3") {

         //let's create tables.
         DBSchema.create;

         import DBSchema._

         val karlMarks = persons insert Person(0,"Karl","Marks");
         val fridrihEngels = persons insert Person(0,"Fridrix","Engels");

         val kapital = books insert Book(0L,"Kapital");
          
         authorship insert Authorship(authorId = karlMarks.id, bookId = kapital.id)
         authorship insert Authorship(authorId = fridrihEngels.id, bookId = kapital.id)

         kapital.authors should have size(_)


         DBSchema.drop;
     }
     
  }
       

}

// vim: set ts=4 sw=4 et:
