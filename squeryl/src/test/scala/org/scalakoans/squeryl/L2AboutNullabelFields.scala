package org.scalakoans.squeryl

import org.functionalkoans.forscala.support._


import org.squeryl._
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;

object L2NullableFieldsScope {

case class Person(val id: Long, 
                  val firstName:String, 
                  val secondName: String,
                  val middleName: Option[String]=None) 
                                                 extends KeyedEntity[Long]
{

 // when we have nullable fields, we must define noarg constructor
 // with all options set to Some(..).  This restriction will gone in
 // squeryl-2.0

 def this() = this(id = -1L,
                   firstName = "", secondName="", middleName=Some(""));

}

object DBSchema extends Schema
{
  val persons = table[Person]
}

};

class L2AboutNullableFields extends KoanSuite
{

  import L2NullableFieldsScope._

  koan("""Nullable fields are marked as optional """) {
     SquerylSupport.withTransaction("t2notnull") {

         //let's create tables.
         DBSchema.create;

         import DBSchema._

         // id's are automatically generated.
         val p1 = persons insert Person(0,"Piter","Pen");
         val p2 = persons insert Person(0,"Vladimir","Lenin",Some("Illich"));

         val withMiddleName = from(persons)(p => where(p.middleName isNotNull)
                                             select(p)).single

         withMiddleName.middleName should be(__)


         val withoutMiddleName = from(persons)(p => where(p.middleName isNull)
                                             select(p)).single

         withoutMiddleName.middleName should be(__)

         DBSchema.drop;
     }
     
  }
       

}

// vim: set ts=4 sw=4 et:
