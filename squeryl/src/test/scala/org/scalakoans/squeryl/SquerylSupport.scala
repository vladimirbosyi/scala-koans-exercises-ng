package org.scalakoans.squeryl

import java.sql._
import org.squeryl._;
import org.squeryl.adapters._;
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;

import scala.concurrent._

object SquerylSupport
{

  def contextScope[A](dbname: String)(f: =>A): A =
   {
    objLock.acquire();
    SessionFactory.concreteFactory = Some( () => 
      Session.create(DriverManager.getConnection("jdbc:h2:mem:"+dbname,"sa",""),
                     new H2Adapter)
    );
    try {
      f
    } finally {
      SessionFactory.concreteFactory = None; 
      objLock.release();
    }
   }

   def withTransaction[A](dbName: String)(f: =>A): A =
     contextScope(dbName)(transaction{f}) ;


   val objLock = new Lock();
   Class.forName("org.h2.Driver");

}

// vim: set ts=4 sw=4 et:
