package org.scalakoans.squeryl

import org.functionalkoans.forscala.support._


import org.squeryl._
import org.squeryl.dsl._;
import org.squeryl.PrimitiveTypeMode._;

object L1SchemaScope {

case class Person(val id: Long, val firstName:String, val secondName: String) 
                                                 extends KeyedEntity[Long];

object DBSchema extends Schema
{
  val persons = table[Person]
}

};

class L1AboutSchema extends KoanSuite
{

  import L1SchemaScope._

  koan("""Simple table mapping""") {
     SquerylSupport.withTransaction("t1") {

         //let's create tables.
         DBSchema.create;

         import DBSchema._

         // id's are automatically generated.
         val p1 = persons insert Person(0,"Karl","Marks");
         val p2 = persons insert Person(0,"Fridrix","Engels");

         (p1.id == p2.id) should be(__)
          
         val allKarls = from(persons)(p => where(p.firstName==="Karl")
                                             select(p))

         allKarls.isEmpty should be(__)

         allKarls foreach { p =>
            p.firstName should be(__)
         }

         // lookup by id return option.
         val x = persons.lookup(-1L);
         x should be(__)

         val x1 = persons.lookup(p1.id);
         x1 should be(__)

         // when we now, that we have one result, we can use .single

         val oneKarl = from(persons)(p => where(p.firstName==="Karl")
                                             select(p)).single

         oneKarl.firstName should be (__)

         persons insert Person(0,"Karl", "Weierstrass")

         allKarls.size should be(__)

         //intercept{
         //}

         DBSchema.drop;
     }
     
  }
       

}

// vim: set ts=4 sw=4 et:
