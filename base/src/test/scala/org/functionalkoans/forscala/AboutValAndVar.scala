package org.functionalkoans.forscala

import support.KoanSuite

class AboutValAndVar extends KoanSuite {

  koan("vars may be reassigned") {
    var a = 5
    a should be(5)

    a = 15
    a should be(15)
  }

  koan("vals may not be reassigned") {
    val a = 8
    a should be(8)

    //What happens if you uncomment these lines?
    val b = 7
    b should be (7)
  }
}
